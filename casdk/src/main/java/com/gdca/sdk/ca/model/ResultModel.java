package com.gdca.sdk.ca.model;

/**
 * Created by Dell on 2017/3/15.
 */

public class ResultModel {
    private long id;
    private String publicKey;
    private int result;//签署结果 0:待办1:通过 2:拒绝 -1:异常
    private int status;//签署状态 0:等待认领 1:待办2:已办4:正在生成缩略图 5:异常

    public ResultModel() {
    }

    /**
     * @param id
     * @param publicKey
     */
    public ResultModel(long id, String publicKey) {
        this.id = id;
        this.publicKey = publicKey;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
