package com.gdca.sdk.ca.listener;

import com.gdca.sdk.ca.model.ResponseContent;

import okhttp3.Call;

/**
 * Created by Dell on 2017/3/16.
 */

public interface RequestListener {
    void onError(int code, Call call, Exception e);

    void onFail(int code, String msg);

    void onSuccess(ResponseContent content);
}
