package com.gdca.sdk.ca.listener;

/**
 * Created by Dell on 2017/3/16.
 */

public abstract class RequestCallBack implements RequestListener {

    public void onBefore() {
    }

    public void onAfter() {
    }

    public void onTimeout(final boolean needRetry, String msg) {
//        try {
//            if (getTopActivity() == null)
//                return;
//            showAlertDialog(null, getTopActivity().getResources().getString(R.string.timeout_msg), null, getTopActivity().getResources().getString(R.string.button_ok), null);
//        } catch (Exception e) {
//
//        }
    }

//    private void showAlertDialog(String title, String message, String negativeMsg, String positiveMsg, final BaseActivity.AlertDialogClickListener listener) {
//        if (getTopActivity() == null || Utils.networkErrorIsOpen)
//            return;
//        Utils.networkErrorIsOpen = true;
//        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getTopActivity());
//        builder.setTitle(title);
//        builder.setMessage(message);
//        if (!StringUtils.isEmpty(negativeMsg)) {
//            builder.setNegativeButton(negativeMsg, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                    if (listener != null) {
//                        listener.cancel();
//                    }
//                }
//            });
//        }
//        if (!StringUtils.isEmpty(positiveMsg)) {
//            builder.setPositiveButton(positiveMsg, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                    if (listener != null) {
//                        listener.ok();
//                    }
//                }
//            });
//        }
//        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                Utils.networkErrorIsOpen = false;
//            }
//        });
//        try {
//            builder.show();
//        } catch (Exception e) {
//            Utils.networkErrorIsOpen = false;
//        }
//    }

}
