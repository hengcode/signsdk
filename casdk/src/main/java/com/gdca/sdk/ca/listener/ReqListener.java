package com.gdca.sdk.ca.listener;

import com.gdca.sdk.ca.model.ResultModel;

/**
 * Created by Dell on 2017/3/15.
 */

public interface ReqListener {
    void onResp(int resultCode, int type, ResultModel result);
}
