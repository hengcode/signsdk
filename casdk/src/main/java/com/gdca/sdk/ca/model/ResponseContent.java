package com.gdca.sdk.ca.model;

/**
 * Created by Dell on 2017/3/16.
 */

public class ResponseContent {
    public final static int CODE_SUCCESS = 0;
    public final static int CODE_TIMEOUT = -2;//token超时

    private int code;
    private String content;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return code == 0 ? true : false;
    }

}
