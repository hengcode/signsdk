package com.gdca.sdk.ca.listener;

/**
 * Created by Dell on 2017/7/4.
 */

public interface GdcaResultListener {
    void onResultSuccess(int code);

    void onResultError(String msg);
}
