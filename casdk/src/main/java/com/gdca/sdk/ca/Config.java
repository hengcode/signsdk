package com.gdca.sdk.ca;

/**
 * Created by Dell on 2017/5/10.
 */

public class Config {
    public final static long DEFULT_TIME_OUT = 20000;

    public final static int OTHER_ERROR = 0;//其他错误
    public final static int FILE_ERROR = 1;//文件读写错误
    public final static int NOT_CHECK_ERROR = 2;//没有校验错误
    public final static int NETWORK_TIMEOUT_ERROR = 3;//网络超时错误
}
