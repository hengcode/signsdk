package com.gdca.sdk.ca.listener;

/**
 * Created by Dell on 2017/5/10.
 */

public interface GdcaLoginListener {
    void onLoginSuccess(int resultCode);

    void onLoginError(int errorCode, String errorMessage);
}
