package com.gdca.sdk.cademo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.gdca.sdk.ca.SdkManager;
import com.gdca.sdk.ca.listener.GdcaLoginListener;
import com.gdca.sdk.ca.listener.GdcaResultListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SdkManager.getInstance().checkLogin(this, new GdcaLoginListener() {
            @Override
            public void onLoginSuccess(int resultCode) {
                Toast.makeText(MainActivity.this, "检验成功", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLoginError(int errorCode, String errorMessage) {
                Toast.makeText(MainActivity.this, "检验失败", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void sign(View view) {
//        SdkManager.getInstance().requestSign(617);
    }

    public void bind(View view) {
        SdkManager.getInstance().requestBind(MainActivity.this, "12346", "test12", "", new GdcaResultListener() {
            @Override
            public void onResultSuccess(int code) {

            }

            @Override
            public void onResultError(String msg) {
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void login(View view) {
        SdkManager.getInstance().requestLogin(MainActivity.this, "", new GdcaResultListener() {
            @Override
            public void onResultSuccess(int code) {

            }

            @Override
            public void onResultError(String msg) {
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}

