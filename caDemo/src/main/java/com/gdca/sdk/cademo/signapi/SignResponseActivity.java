package com.gdca.sdk.cademo.signapi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.gdca.sdk.ca.listener.ReqListener;
import com.gdca.sdk.ca.model.ResultModel;
import com.gdca.sdk.ca.SdkManager;
import com.gdca.sdk.cademo.R;


public class SignResponseActivity extends AppCompatActivity implements ReqListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_respon);
        SdkManager.getInstance().handleIntent(this, getIntent(), this);
    }

    @Override
    public void onResp(int resultCode, int type, ResultModel result) {
        TextView view = (TextView) findViewById(R.id.tv_text);
        if (type == SdkManager.TYPE_SIGN) {
            view.setText("签署  resultCode : " + resultCode);
        } else if (type == SdkManager.TYPE_BIND) {
            view.setText("绑定  resultCode : " + resultCode);
        } else if (type == SdkManager.TYPE_LOGIN) {
            view.setText("登录  resultCode : " + resultCode);
        }
        Log.e("onResp", "resultCode : " + resultCode);
        if (result != null) {
            view.setText("pushKey : " + result.getPublicKey());
        }
    }
}
